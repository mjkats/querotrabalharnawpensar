from rest_framework import generics, permissions

from ..models import Produto
from ..serializers import ProdutoSerializer

class ProdutoList(generics.CreateAPIView):
    """
    Create Produto
    """
    queryset = Produto.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProdutoSerializer