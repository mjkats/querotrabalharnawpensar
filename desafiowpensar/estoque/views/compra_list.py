from ..serializers import CompraSerializerList
from ..serializers import CompraSerializerPost
from ..models import Compra

from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_spectacular.utils import extend_schema

class CompraList(APIView):
    """
    CompraListTeste
    """

    permission_classes = [permissions.IsAuthenticated]
    
    def get(self, request, *args, **kwargs):
        compras = Compra.objects.all()
        serializer = CompraSerializerList(compras, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CompraSerializerPost(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)