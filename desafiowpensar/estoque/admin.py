from django.contrib import admin

from .models import Compra
from .models import Produto
from .models import Lote

# Register your models here.
admin.site.register(Compra)
admin.site.register(Produto)
admin.site.register(Lote)