from djmoney.models.fields import MoneyField

from django.db import models

class Compra(models.Model):
    preco_total = models.DecimalField(decimal_places=2, max_digits=11, null=True)