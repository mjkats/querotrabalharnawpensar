from django.db import models

from .produto import Produto
from .compra import Compra

class Lote(models.Model):
    compra = models.ForeignKey(Compra, on_delete=models.CASCADE, related_name='lotes')
    quantidade = models.IntegerField()
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
    preco_lote = models.DecimalField(decimal_places=2, max_digits=9)
    preco_medio = models.DecimalField(decimal_places=2, max_digits=7, null=True)