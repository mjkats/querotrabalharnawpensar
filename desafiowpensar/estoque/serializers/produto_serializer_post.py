from ..models import Produto

from rest_framework import serializers

class ProdutoSerializerPost(serializers.ModelSerializer):
    class Meta:
        model = Produto

        fields = ["pk"]