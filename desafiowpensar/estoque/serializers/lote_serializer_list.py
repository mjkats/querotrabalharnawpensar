from ..models import Lote
from .produto_serializer_list import ProdutoSerializerList

from rest_framework import serializers

class LoteSerializerList(serializers.ModelSerializer):

    produto = ProdutoSerializerList()

    def create(self, validated_data):
        return Compra.objects.create(**validated_data)

    class Meta:
        model = Lote

        fields = [
            "quantidade",
            "preco_lote",
            "preco_medio",
            "produto"
        ]
