from ..models import Compra
from .lote_serializer_list import LoteSerializerList

from rest_framework import serializers

class CompraSerializerList(serializers.ModelSerializer):

    lotes = LoteSerializerList(many = True)

    class Meta:
        model = Compra

        fields = [
            "preco_total",
            "lotes"
            ]