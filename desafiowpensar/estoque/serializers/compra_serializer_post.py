from ..models import Compra
from ..models import Lote
from .lote_serializer_post import LoteSerializerPost

from rest_framework import serializers

class CompraSerializerPost(serializers.ModelSerializer):

    lotes = LoteSerializerPost(many = True)

    class Meta:
        model = Compra

        fields = [
            "lotes"
            ]

    def create(self, validated_data):
        lotes_data = validated_data.pop('lotes')
        compra = Compra.objects.create(**validated_data)
        preco_total_compra = 0

        for lote_data in lotes_data:
            preco_total_compra += lote_data["preco_lote"]
            lote_data["preco_medio"] = lote_data["preco_lote"] / lote_data["quantidade"]
            Lote.objects.create(compra=compra, **lote_data)
            Compra.objects.filter(pk=compra.pk).update(preco_total=preco_total_compra)
            
        return compra