from ..models import Lote
from .produto_serializer_post import ProdutoSerializerPost

from rest_framework import serializers

class LoteSerializerPost(serializers.ModelSerializer):

    class Meta:
        model = Lote

        fields = [
            "quantidade",
            "preco_lote",
            "produto"
        ]
