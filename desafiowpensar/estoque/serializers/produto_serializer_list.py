from ..models import Produto

from rest_framework import serializers

class ProdutoSerializerList(serializers.ModelSerializer):
    class Meta:
        model = Produto

        fields = ["nome"]