# **Ambiente Utilizado:** #

1. Utilização do Python versão 3.8.5, onde as versões 3.7 para cima funcionarão normalmente
3. Utilização do MySQL database
2. Utilização do Pip3 para gerenciar as bibliotecas do próximo passo
3. Bibliotecas a serem instaladas via pip: Django, djangorestframework, drf_spectacular, mysqlclient
4. Insomnia para testar requisições HTTP

# **Configuração do Ambiente:** #

1. Criar via mysql um banco de dados com o nome "wpensar" (sem as aspas)
2. Utilizar um usuario criado anteriormente ou criar um novo no mysql e fornecer acesso do BD wpensar para o mesmo
3. Em /desafiowpensar/settings.py alterar as values das keys "USER" e "PASSWORD", na variavel DATABASES, para o usuario e senha associados com o DB
4. Se necessário, alterar também a value referente à key "PORT" para utilizar uma outra porta
5. Uma vez no diretório /desafiowpensar, execute o comando 'python3 manage.py createsuperuser' (sem as aspas) e crie um usuario do sistema
8. Execute o comando 'python3 manage.py runserver' (sem as aspas) para rodar o sistema
9. Acessando o link fornecido no terminal ( normalmente http://127.0.0.1:8000/ ), acesse em um browser o link http://127.0.0.1:8000/admin
10. Faça o login de acordo com o superuser criado
11. Em "Tokens", clique em "+ Add"
12. Na dropdown list "User", selecione seu usuario e clique em "SALVAR"
13. Copie a chave demonstrada
14. Para fazer as requisições HTTP, será necessário fornecer essa chave no campo 'TOKEN' da autorização 'Bearer'. No prefixo, adicione 'Token'
15. Faça a requisição HTTP do sistema que desejar.

# **Documentação:** #

Presente via swagger na url /docs, porém a mesma está incompleta.

Schema presente na url /schema

# **Desafio:** #

O conceito desse desafio é nos ajudar a avaliar as habilidades dos candidatos às vagas de backend.

Você tem que desenvolver um sistema de estoque para um supermercado.

Esse supermercado assume que sempre que ele compra uma nova leva de produtos, ele tem que calcular o preço médio de compra de cada produto para estipular um preço de venda.
Para fins de simplificação assuma que produtos que tenham nomes iguais, são o mesmo produto e que não existe nem retirada e nem venda de produtos no sistema.

O valor calculado de preço médio deve ser armazenado.

Seu sistema deve:

1. [x] Cadastro de produtos (Nome)
2. [x] Compra de produtos (Produto, quantidade e preço de compra)
3. [x] Listagem dos produtos comprados separados por compra (Nome, quantidade, preço de compra, preço médio)
4. [x] Ser fácil de configurar e rodar em ambiente Unix (Linux ou Mac OS X)
5. [x] Ser WEB
6. [x] Ser escrita em Python 3.7+
7. [x] Só deve utilizar biliotecas livres e gratuitas

Esse sistema não precisa ter, mas será um plus:

1. [x] Autenticação e autorização (se for com OAuth, melhor ainda)
2. [ ] Ter um design bonito
3. [ ] Testes automatizados
4. [ ] Usar Jinja Template Engine para fazer o Front End.
5. [ ] Disponibilizar o Projeto na Nuvem(Heroku, Aws, ou similar)

# **Avaliação:** #

Vamos avaliar seguindo os seguintes critérios:

1. Você conseguiu concluir os requisitos?

Concluí os requisitos obrigatórios, e, dentre os extras, apenas a autenticação.

2. Você documentou a maneira de configurar o ambiente e rodar sua aplicação?

Sim, como encontra-se na sessão "Ambiente Utilizado".
